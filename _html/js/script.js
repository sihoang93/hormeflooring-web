var script = function () {

    var win = $(window);

    var mMenu = function () {
        var menu = $('.m-nav');
        var ct = menu.find('.nav-ct');
        var open = $('.nav-open');
        var close = $('.nav-close');

        ct.append($('.main-option').clone());
        ct.append($('.form-search').clone());
        ct.append($('.main-nav').clone());
        ct.append($('.head-info').clone());
        ct.append($('.head-social').clone());


        open.click(function (e) {
            e.preventDefault();
            if (win.width() < 1199) {
                menu.addClass('act');
            }
        });
        close.click(function (e) {
            e.preventDefault();
            if (win.width() < 1199) {
                menu.removeClass('act');
            }
        });


        var sidebar = $('.sb-nav');
        var open2 = $('.sb-open');
        var close2 = $('.sb-close');

        open2.click(function (e) {
            e.preventDefault();
            if (win.width() < 1199) {
                sidebar.addClass('act');
            }
        });
        close2.click(function (e) {
            e.preventDefault();
            if (win.width() < 1199) {
                sidebar.removeClass('act');
            }
        });

        win.click(function (e) {
            if (menu.has(e.target).length == 0 && !menu.is(e.target) && open.has(e.target).length == 0 && !open.is(e.target)) {
                menu.removeClass('act');
            }
            if (sidebar.has(e.target).length == 0 && !sidebar.is(e.target) && open2.has(e.target).length == 0 && !open2.is(e.target)) {
                sidebar.removeClass('act');
            }
        });


        nav = menu.find('.main-nav');
        nav.find("ul li").each(function () {
            if ($(this).find("ul>li").length > 0) {
                $(this).prepend('<i class="nav-drop"></i>');
            }
        });

        $(".nav-drop").click(function () {
            var ul = $(this).nextAll(".sub-menu");
            if (ul.is(":hidden") === true) {
                ul.parent('li').parent().children().children('ul').slideUp(0);
                ul.parent('li').parent().children().children('.nav-drop').removeClass("act");
                $(this).addClass("act");
                // ul.slideDown(200);
                ul.css('display', 'flex');
            } else {
                ul.slideUp(0);
                $(this).removeClass("act");
            }
        });
    };

    var uiSlick = function () {
        // Top page
        $('.comments-slick').slick({
            arrows: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 3000,
            infinite: true,
            slidesToShow: 1,
        });
        $('.row-products-st').slick({
            nextArrow: '<i class="icofont-swoosh-right smooth next"></i>',
            prevArrow: '<i class="icofont-swoosh-left smooth prev"></i>',
            arrows: true,
            dots: false,
            autoplay: false,
            autoplaySpeed: 3000,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
            ]
        });




        $('.spSlick').on('init', function (event, slick) {
            $(this).append('<div class="slick-counter">0<span class="current"></span> / 0<span class="total"></span></div>');
            $('.current').text(slick.currentSlide + 1);
            $('.total').text(slick.slideCount);
        })
            .slick({
                nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                arrows: true,
                lazyLoad: 'ondemand',
                speed: 1000,
                dots: true,
                swipeToSlide: true,
                centerPadding: '50px',
                dotsClass: 'custom_paging',
                customPaging: function (slider, i) {
                    return (i + 1) + '/' + slider.slideCount;
                },
                autoplay: true,
                autoplaySpeed: 3000,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                    }
                }]
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.current').text(nextSlide + 1);
            });

        //Slide Dự án
        $('.duSlick').on('init', function (event, slick) {
            $(this).append('<div class="slickDa-counter">0<span class="dacurrent"></span> / 0<span class="datotal"></span></div>');
            $('.dacurrent').text(slick.currentSlide + 1);
            $('.datotal').text(slick.slideCount);
        })
            .slick({
                nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                arrows: true,
                dots: true,
                autoplay: true,
                autoplaySpeed: 3000,
                lazyLoad: 'ondemand',
                speed: 1000,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.dacurrent').text(nextSlide + 1);
            });

        //Dự án chi tiết
        $("#slider01").slick({
            slidesToScroll: 1,
            slidesToShow: 1,
            autoplay: true,
            arrows: false,
            fade: true,
            asNavFor: "#thumbnail_slider01",
        });
        $('#thumbnail_slider01').on('init', function (event, slick) {
            $(this).append('<div class="slickDa-counter">0<span class="dacurrent"></span> / 0<span class="datotal"></span></div>');
            $('.dacurrent').text(slick.currentSlide + 1);
            $('.datotal').text(slick.slideCount);
        })
        .slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            arrows: true,
            dots: true,
            asNavFor: "#slider01",
            focusOnSelect: true
        })
        .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            $('.dacurrent').text(nextSlide + 1);
        });


        //Dự án liên quan
        $('.daCt02Slide').on('init', function (event, slick) {
            $(this).append('<div class="slickDa-counter">0<span class="dacurrent"></span> / 0<span class="datotal"></span></div>');
            $('.dacurrent').text(slick.currentSlide + 1);
            $('.datotal').text(slick.slideCount);
        })
            .slick({
                nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                arrows: true,
                dots: true,
                autoplay: true,
                autoplaySpeed: 3000,
                lazyLoad: 'ondemand',
                speed: 1000,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.dacurrent').text(nextSlide + 1);
            });


        //Hoạt động chi tiết
        $('.hdCtSlick').on('init', function (event, slick) {
            $(this).append('<div class="slickhd-counter">0<span class="hdcurrent"></span> / 0<span class="hdtotal"></span></div>');
            $('.hdcurrent').text(slick.currentSlide + 1);
            $('.hdtotal').text(slick.slideCount);
        })
            .slick({
                nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                arrows: true,
                dots: true,
                autoplay: true,
                autoplaySpeed: 3000,
                lazyLoad: 'ondemand',
                speed: 1000,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.hdcurrent').text(nextSlide + 1);
            });


        //Đối tác
        $('.dtSlick').on('init', function (event, slick) {
            $(this).append('<div class="slickDt-counter">0<span class="dtcurrent"></span> / 0<span class="dttotal"></span></div>');
            $('.dtcurrent').text(slick.currentSlide + 1);
            $('.dttotal').text(slick.slideCount);
        })
            .slick({
                nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                arrows: true,
                dots: true,
                autoplay: true,
                autoplaySpeed: 3000,
                lazyLoad: 'ondemand',
                speed: 1000,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.dtcurrent').text(nextSlide + 1);
            });

        $('#slide').slick({
            nextArrow: '<i class="icofont-swoosh-right smooth next"></i>',
            prevArrow: '<i class="icofont-swoosh-left smooth prev"></i>',
            arrows: true,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3000,
            infinite: true,
            lazyLoad: 'ondemand',
            speed: 1000,
        });


    };

    var uiHover = function () {

        // var className = $('#why .col-right ul li');
        // className.mouseover(function () {    
        //     $(this).find('img').attr("src", $(this).find('img').attr("src").replace(".png", "-hv.png"));
        // }).mouseout(function () {
        //     $(this).find('img').attr("src", $(this).find('img').attr("src").replace("-hv.png", ".png"));
        // });

        $('#procedure .item').hover(function () {
            $(this).prev().find('.icon').addClass('hover');
        }, function () {
            $(this).prev().find('.icon').removeClass('hover');
        });
    };

    var uiToggle = function () {

        $('.btn-search').click(function () {
            if ($('.form-search').hasClass('active')) {
                $('.form-search').removeClass('active');
            } else {
                $('.form-search').addClass('active');
            }
        });

        $('#faqs .item').click(function (e) {
            e.preventDefault();
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
                $(this).closest('.item').find('.content').slideUp(300);
                $(this).find('i').removeClass('fa-minus').addClass('fa-plus');
            } else {
                $(this).addClass('open');
                $(this).closest('.item').find('.content').slideDown(300);
                $(this).find('i').removeClass('fa-plus').addClass('fa-minus');
            }
        });

        $('#why .col-right ul li').hover(function () {
            $(this).addClass('act');
            $(this).find('img').attr("src", $(this).find('img').attr("src").replace(".png", "-hv.png"));
        }, function () {
            $(this).removeClass('act');
            $(this).find('img').attr("src", $(this).find('img').attr("src").replace("-hv.png", ".png"));
        });

    };

    var backToTop = function () {
        var back_top = $('.back_to_top');

        if (win.scrollTop() > 600) {
            back_top.fadeIn();
        }

        back_top.click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        win.scroll(function () {
            if (win.scrollTop() > 600) back_top.fadeIn();
            else back_top.fadeOut();
        });
    };

    var uiClick = function () {
        $('#filterOptions li a').click(function () {
            var ourClass = $(this).attr('data-filter');
            $('#filterOptions li').removeClass('active');
            $(this).parent().addClass('active');
            if (ourClass == 'all') {
                $('.row-product').children('div.col-item').show();
            } else {
                $('.row-product').find('div.col-item').hide()
                    .filter(function () {
                        return $(this).data('filter') === ourClass;
                    })
                    .show();
            }
            return false;
        });
    }


    var uiCoHoi = function () {
        $(".ch02IChild h3").click(function () {
            $(".ch02IChild h3").removeClass('active');
            $(".ch02IChild .ch02FullTxt").hide('show');
            $(this).next().stop().slideToggle();
            $(this).toggleClass("active");
        });
    }

    var uiTab = function () {
        $('.tab-link li:first-child a').addClass('active');
        $('.tab-content .item:first-child').addClass('active');
        $('.tab-link li a').click(function () {
            $('.tab-link li a').removeClass('active');
            $(this).addClass('active');
            $('.item').removeClass('active');
            $('#content-' + $(this).attr('id')).addClass('active');
        });
    }


    return {

        uiInit: function ($fun) {
            switch ($fun) {
                case 'mMenu':
                    mMenu();
                    break;
                case 'uiSlick':
                    uiSlick();
                    break;
                case 'uiHover':
                    uiHover();
                    break;
                case 'uiToggle':
                    uiToggle();
                    break;
                case 'backToTop':
                    backToTop();
                    break;
                case 'uiCoHoi':
                    uiCoHoi();
                    break;
                case 'uiClick':
                    uiClick();
                    break;
                default:
                    mMenu();
                    uiSlick();
                    uiHover();
                    uiToggle();
                    backToTop();
                    uiCoHoi();
                    uiClick();
            }
        }

    };

}();


jQuery(function ($) {
    var wow = new WOW({
        offset: 50,
        mobile: false
    });
    wow.init();
    script.uiInit();
});

var width = $(window).width();



function parallaxImg() {
    var winY = $(this).scrollTop();
    var imgPercent = winY * 0.05;
    $('.img-parallax').css({
        top: imgPercent + '%',
        transform: 'translate(-50%, -' + imgPercent + '%)'
    });
}

$(document).ready(function () {

    scrollElr(width);
    parallaxImg();
    // Increase quantity
    $(".incr-btn").on("click", function (e) {
        var oldValue = $(this).parent().find('.quantity').val();
        $(this).parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($(this).data('action') == "increase") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $(this).addClass('inactive');
            }
        }
        $(this).parent().find('.quantity').val(newVal);
        e.preventDefault();
    });

    // Check label
    $(".color label").on("click", function (e) {
        $(".color label").removeClass('check');
        $(this).addClass('check');
        e.preventDefault();
    });


    if ($('.product__slider-main').length) {
        var $slider = $('.product__slider-main')
            .on('init', function (slick) {
                $('.product__slider-main').fadeIn(1000);
            })
            .slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                autoplay: true,
                lazyLoad: 'ondemand',
                autoplaySpeed: 3000,
                asNavFor: '.product__slider-thmb'
            });

        var $slider2 = $('.product__slider-thmb')
            .on('init', function (slick) {
                $('.product__slider-thmb').fadeIn(1000);
            })
            .slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                lazyLoad: 'ondemand',
                asNavFor: '.product__slider-main',
                dots: false,
                centerMode: false,
                focusOnSelect: true
            });

        //remove active class from all thumbnail slides
        $('.product__slider-thmb .slick-slide').removeClass('slick-active');

        //set active class to first thumbnail slides
        $('.product__slider-thmb .slick-slide').eq(0).addClass('slick-active');

        // On before slide change match active thumbnail to current slide
        $('.product__slider-main').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            var mySlideNumber = nextSlide;
            $('.product__slider-thmb .slick-slide').removeClass('slick-active');
            $('.product__slider-thmb .slick-slide').eq(mySlideNumber).addClass('slick-active');
        });

    }


    var getData = $('.custom_paging li');
    getData.each(function (i) {
        console.log($(this).html());
    });


    // $('.spItem').blur(function(){
    //     console.log('1');
    // });

    var getItem = $('.spItem');
    getItem.each(function (i) {
        //console.log(i);
        getItem.change(function (j) {
            console.log(j);
        });
    });


    // Inline popups
    $('.inline-popups').magnificPopup({
        delegate: 'a',
        removalDelay: 500, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function () {
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        },
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

});

$(window).resize(function () {
    width = $(window).width();
    scrollElr(width);
});

$( document ).ready(function() {
  
    var options = {
        loop: true,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'], 
        autoplay: true,
        navSpeed: 200,
        dots: true,
        dotsSpeed: 800,
        margin: 30,
        responsive: {
         0:{
            items: 2,
            margin: 5,
            slideBy: 'page'
            },
          767: {
            items: 4,
            slideBy: 'page'
          }
        },
        slideBy: 'page',
        lazyLoad: true,
        onDrag: onDrag.bind(transient),
        onDragged: onDragged.bind(transient)
    };
    
    // Empty object where we can store current item's index before drag
    var transient = {};
    

   
    $('.slide-pc .top-slide-item').owlCarousel(options);
    function onDrag(event) {
      this.initialCurrent = event.relatedTarget.current();
    }
    
    function onDragged(event) {
      var owl = event.relatedTarget;
      var draggedCurrent = owl.current();
    
      if (draggedCurrent > this.initialCurrent) {
        owl.current(this.initialCurrent);
        owl.next();
      } else if (draggedCurrent < this.initialCurrent) {
        owl.current(this.initialCurrent);
        owl.prev();
      }
    }
});

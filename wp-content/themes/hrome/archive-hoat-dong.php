<?php get_header();?>
<?php get_template_part( 'inc/header', 'hoatdong' ); ?>
<!-- Main -->
<!-- Main -->
<main>
    <section id="hoatDongPage">
        <div class="grid-1175">
        <?php $i = 1; $j = 1; if ( have_posts() ) : ?>
            <div class="hdRows wow fadeInUp">
            <?php while ( have_posts() ) : the_post();
            //echo $j.'<br />';
                $rs = substr(strrchr($j/3, "."), 1);
            //echo $rs;
            ?>  
                <div class="<?php if( $rs == '33333333333333' ||  $rs == '3333333333333' ) { echo 'hdLeft'; } else if( $rs == '66666666666667' || $rs == '6666666666667') { echo 'hdMid'; } else if( $rs == '0' ) { echo 'hdRight'; } ?>"  <?php if( $rs == '33333333333333' ||  $rs == '3333333333333' ):?> style="background-image: url('<?php if ( has_post_thumbnail() ){ the_post_thumbnail_url(); } else { echo get_stylesheet_directory_uri().'/images/hd-img01.jpg'; } ?>');" <?php endif; ?>>
                    <?php if( $rs == '66666666666667' || $rs == '6666666666667' || $rs == '0'): ?>
                        <?php if ( has_post_thumbnail() ) { ?>
                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <?php } else { ?>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hd-img02.jpg" alt="<?php the_title(); ?>">
                        <?php } ?>
                    <?php endif; ?>
                    <div class="hdInfo ow">
                        
                        <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <span class="txt">
                            <?php echo mb_substr(get_the_excerpt(), 0, 120,'UTF-8').'...'; ?>
                        </span>
                        <span class="date"><?php the_time('d F, Y'); ?></span>
                    </div>
                </div>
                <?php if(  $i%3 == 0  ) { 
                    echo '</div><div class="hdRows wow fadeInUp">';}  ?>
                <?php  $i++; $j++; endwhile; ?>
            </div>
            <div class="wrap-pagi">
                <?php wp_pagenavi(); /* Vị trí hiển thị phân trang */?> 
            </div>
                <?php  wp_reset_query(); ?>
            <?php  endif; ?>
        </div>
    </section>
</main>
<!-- End main -->
<!-- End main -->
<?php get_footer(); ?>
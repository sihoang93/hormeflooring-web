<form id="contact_form" name="contact_form" method="post"
    action="<?php echo get_template_directory_uri(); ?>/hoangsimail/sendmail.php">

    <!-- required -->
    <input type="hidden" name="hoangsi_email_admin" value="<?php the_field('dia_chi_email', 'option'); ?>">
    <input type="hidden" name="hoangsi_blog_name" value="<?php bloginfo('name');?>">
    <input type="hidden" name="hoangsi_home_url" value="<?php echo home_url(); ?>">
    <input type="hidden" name="hoangsi_url_contact" value="<?php global $wp; echo home_url( $wp->request ); ?>">
    <!-- required -->
    <div class="xeDRFRows">
        <div class="xeDFRLeft">
            <span class="fTT">ĐIỀN THÔNG TIN ĐỂ NHẬN TƯ VẤN MUA</span>
            <table>
                <tr>
                    <td>Họ và tên: </td>
                    <td><input type="text" name="hoangsi_name" class="form-control" required></td>
                </tr>
                <tr>
                    <td>Số điện thoại: </td>
                    <td><input type="tel" name="hoangsi_tel" class="form-control" required></td>
                </tr>
            </table>
        </div>
        <div class="xeDFRRight">
            <input type="submit" value="Gửi">
        </div>
    </div>
</form>
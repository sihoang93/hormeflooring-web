<div class="normal-post">
    <a href="<?php the_permalink() ?>" class="hv-o">
        <?php if ( has_post_thumbnail() ) { ?>
        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
        <?php } else { ?>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/post-1.jpg" alt="<?php the_title(); ?>">
        <?php } ?>
    </a>
    <div class="post-header">
        <h3 class="featured-post-title"><a href="<?php the_permalink() ?>" class="link"><?php the_title(); ?></a></h3>
        <span class="post-date"><?php the_time( 'F,d,Y') ?></span>
        <div class="circle-line">
            <i class="fa fa-circle-o"></i>
            <i class="fa fa-circle-o"></i>
            <i class="fa fa-circle-o"></i>
        </div>
        <ul>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
        </ul>
        <div class="post-content">
            <p>
                <?php echo mb_substr(get_the_excerpt(), 0, 220,'UTF-8').'...'; ?>
            </p>
            <div class="post-footer">
                <span class="post-category"><?php the_category( ' ' ); ?></span><span><a href="<?php the_permalink() ?>">Xem thêm <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a></span>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer>
    <div class="ftRows01">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 ftInfo01">
                    <span class="logoFt"><a href="<?php bloginfo('home'); ?>" class="hv-o"><img
                                src="<?php echo get_template_directory_uri() ?>/images/logoFt.png" alt=""></a></span>
                    <ul class="ftInfo">
                        <li><span class="ftIcon"><i class="fa fa-phone" aria-hidden="true"></i></span> <span
                                class="ftTxt"><a
                                    href="<?php $tel = get_field('số_diện_thoại','options'); $tel_result1 = trim($tel); $tel_result2 = str_replace(".","",$tel_result1); $tel_result3 = str_replace("-","",$tel_result2); $tel_result4 = str_replace(" ","",$tel_result3); echo 'tel:'.$tel_result4; ?>"><?php the_field('số_diện_thoại', 'options'); ?></a></span>
                        </li>
                        <li><span class="ftIcon"><i class="fa fa-envelope" aria-hidden="true"></i></span> <span
                                class="ftTxt"><a
                                    href="mailto:i<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></span>
                        </li>
                        <li><span class="ftIcon"><i class="fa fa-map-marker" aria-hidden="true"></i></span> <span
                                class="ftTxt">C<?php the_field('dịa_chỉ', 'options'); ?></span></li>
                    </ul>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-4">
                            <h3><?php _e("<!--:en-->Information<!--:--><!--:vi-->Thông tin<!--:-->"); ?></h3>
                            <span class="FtNav">
                                <a
                                    href="<?php bloginfo('home'); ?>/gioi-thieu/"><?php _e("<!--:en-->About<!--:--><!--:vi-->Giới thiệu<!--:-->"); ?></a><br>
                                <a
                                    href="<?php bloginfo('home'); ?>/co-hoi-nghe-nghiep/"><?php _e("<!--:en-->Papal opportunity<!--:--><!--:vi-->Cơ hội nghề nghiệp<!--:-->"); ?></a><br>
                                <a
                                    href="<?php bloginfo('home'); ?>/hoat-dong-cua-chung-toi/"><?php _e("<!--:en-->Our activities<!--:--><!--:vi-->Hoạt động của chúng tôi<!--:-->"); ?></a><br>
                                <a
                                    href="<?php bloginfo('home'); ?>"><?php _e("<!--:en-->Living space<!--:--><!--:vi-->Không gian sống<!--:-->"); ?></a><br>
                                <a
                                    href="<?php bloginfo('home'); ?>/du-an/"><?php _e("<!--:en-->Project<!--:--><!--:vi-->Dự án<!--:-->"); ?></a>
                            </span>
                        </div>
                        <div class="col-sm-4">
                            <h3><?php _e("<!--:en-->Product<!--:--><!--:vi-->Sản phẩm<!--:-->"); ?></h3>
                            <span class="FtNav">
                                <a
                                    href="<?php bloginfo('home'); ?>/san-pham/"><?php _e("<!--:en-->Technical wood flooring<!--:--><!--:vi-->Sàn gỗ kỹ thuật<!--:-->"); ?></a><br>
                                <a
                                    href="<?php bloginfo('home'); ?>/san-pham/"><?php _e("<!--:en-->Wood floor accessories<!--:--><!--:vi-->Phụ kiện sàn gỗ<!--:-->"); ?></a><br>
                            </span>
                        </div>
                        <div class="col-sm-4">
                            <h3><?php _e("<!--:en-->Distribution System<!--:--><!--:vi-->Hệ thống phân phối<!--:-->"); ?>
                            </h3>
                            <span class="FtNav">
                                <?php if( have_rows('hệ_thong_phan_phối', 'options') ): while ( have_rows('hệ_thong_phan_phối', 'options')) : the_row(); ?>
                                <a href="<?php the_sub_field('link_dịa_chỉ_thanh_phố', 'options'); ?>"
                                    target="_blank"><?php the_sub_field('ten_thanh_phố', 'options'); ?></a><br>
                                <?php endwhile; endif; ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ftRows02">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="cpRight">
                        ©Copyrigh 2020 <?php the_field('ten_cong_ty', 'options'); ?>. All Rights Reserved.
                    </div>
                    <div class="cpSo">
                        <ul>
                            <li><a href="<?php the_field('link_facebook', 'options'); ?>" target="_blank"
                                    class="hv-o"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="<?php the_field('link_youtube', 'options'); ?>" target="_blank" class="hv-o"><i
                                        class="fa fa-youtube-play" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="<?php the_field('link_twitter', 'options'); ?>" target="_blank" class="hv-o"><i
                                        class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End footer -->
<!-- Page top -->
<div class="back_to_top">
    <a href="javascript:void(0)" class="hvr-pulse"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
</div>

<!-- Add option -->
<div class="anim-phone">
    <!-- <div class="anim-alo-ph-circle"></div>
    <div class="anim-alo-ph-circle-fill"></div>
    <div class="anim-alo-ph-img-circle"><a
            href="<?php $tel = get_field('số_diện_thoại','options'); $tel_result1 = trim($tel); $tel_result2 = str_replace(".","",$tel_result1); $tel_result3 = str_replace("-","",$tel_result2); $tel_result4 = str_replace(" ","",$tel_result3); echo 'tel:'.$tel_result4; ?>"><i
                class="fa fa-phone" aria-hidden="true"></i></a>
    </div> -->
    <a href="http://m.me/" target="_blank" class="hv-o"><img
            src="<?php echo get_template_directory_uri() ?>/images/mess.png" alt="Messenger"></a>
</div>

<!-- Javascript -->
<script src="<?php echo get_template_directory_uri() ?>/js/jquery-2.2.1.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/wow.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/social.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/script.js"></script>
<script>
$(".sub-menu ul li").hover(
    function() {
        $(this).addClass("active");
    },
    function() {
        $(this).removeClass("active");
    }
);

$(".main-nav > ul > li").hover(
    function() {
        $(".sub-menu ul li:first-child", this).addClass("active");
    },
    function() {
        $(".sub-menu ul li:first-child", this).removeClass("active");
    }
);

$('.spSlick4').on('init', function(event, slick) {
        $(this).append(
            '<div class="slick-counter">0<span class="current"></span> / 0<span class="total"></span></div>');
        $('.current').text(slick.currentSlide + 1);
        $('.total').text(slick.slideCount);
    })


    .slick({
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        arrows: true,
        lazyLoad: 'ondemand',
        speed: 1000,
        dots: true,
        swipeToSlide: true,
        centerPadding: '50px',
        dotsClass: 'custom_paging',
        customPaging: function(slider, i) {
            return (i + 1) + '/' + slider.slideCount;
        },
        autoplay: false,
        autoplaySpeed: 3000,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
            }
        }]
    })
    .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $('.current').text(nextSlide + 1);
    });


$('#slick1').on('init', function(event, slick) {
    $(this).append(
        '<div class="slick-counter">0<span class="current"></span> / 0<span class="total"></span></div>');
    $('.current').text(slick.currentSlide + 1);
    $('.total').text(slick.slideCount);
})



if ($(window).width() < 651) {
    $('#slick1').slick({
        dots: true,
        slidesPerRow: 1,
        rows: 2,
        margin: 0,
        speed: 1000,
        autoplay: false,
        autoplaySpeed: 3000,
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        swipeToSlide: true,
        centerPadding: '50px',
        dotsClass: 'custom_paging',
        customPaging: function(slider, i) {
            return (i + 1) + '/' + slider.slideCount;
        }
    });
} else {
    $('#slick1').slick({
        dots: true,
        slidesPerRow: 3,
        rows: 3,
        margin: 0,
        speed: 1000,
        autoplay: false,
        autoplaySpeed: 3000,
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        swipeToSlide: true,
        centerPadding: '50px',
        dotsClass: 'custom_paging',
        customPaging: function(slider, i) {
            return (i + 1) + '/' + slider.slideCount;
        }
    });
};

$('#slick1').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    $('.current').text(nextSlide + 1);
});


</script>
<?php 
    if ( is_front_page()) {?>
    <script>
        function scrollElr(width) {
            if (width > 1184) {
                $('#slide, #breadcrumb').css({
                    'margin-top': 0
                });
                $(window).on('scroll.scrollElr', function() {
                    if ($(this).scrollTop() > $('#slide').outerHeight() - 100) {
                        $('.wrapHeader').addClass("fixed");
                    } else {
                        $('.wrapHeader').removeClass("fixed");
                    }
                });
            } else {
                $(window).off("scroll.scrollElr");
                $('#slide, #breadcrumb').css({
                    'margin-top': '70px'
                });
            }
        }
    </script>
    <?php }
    else{ ?>
     <script>
        function scrollElr(width) {
			if (width > 1184) {
				$('#slide, #breadcrumb').css({
					'margin-top': 0
				});
				$(window).on('scroll.scrollElr', function () {
					if ($(this).scrollTop() > $('.bannerPage').outerHeight() - 80) {
						$('.wrapHeader').addClass("fixed");
					} else {
						$('.wrapHeader').removeClass("fixed");
					}
				});
			} else {
				$(window).off("scroll.scrollElr");
				$('#slide, #breadcrumb').css({
					'margin-top': '70px'
				});
			}
		}
    </script>
    <?php } ?>
<?php wp_footer(); ?>
</body>

</html>
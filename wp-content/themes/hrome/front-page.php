<?php get_header();?>
<!-- Slide -->
<div id="slide">
    <div id="product__slider">
        <div class="product__slider-main">
            <?php if( have_rows('slide_show') ): while ( have_rows('slide_show')) : the_row(); ?>
                <div class="slide" style="background-image: url(<?php the_sub_field('hinh_ảnh_slide'); ?>);">
                    <div class="txtOnSlide"><?php the_sub_field('tieu_dề_lớn_tren_slide'); ?></div>
                </div>
            <?php endwhile; endif; ?>

        </div>
        <div class="product__slider-thmb">
            <?php if( have_rows('slide_show') ): while ( have_rows('slide_show')) : the_row(); ?>
                <div class="slide">
                    <h3><?php the_sub_field('tieu_dề_nhỏ_slide'); ?></h3>
                    <span class="slTxt">
                        <?php the_sub_field('doạn_mo_tả_ngắn'); ?>
                    </span>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</div>
<!-- End slide -->


<!-- Main -->
<main>

    <div id="gioiThieu">
        <div class="container wow fadeInUp">
            <h2><?php the_field('tieu_dề_thuong_hiệu_horme'); ?></h2>
            <span class="gtImg"><img src="<?php the_field('hinh_ảnh_thuong_hiệu_horme'); ?>" alt="<?php the_field('tieu_dề_thuong_hiệu_horme'); ?>"></span>
            <h3><?php the_field('tieu_dề_mo_tả_thuong_thiệu_horme'); ?></h3>
            <div class="gtInfo">
                <?php the_field('mo_tả_ngắn_thuong_hiệu_horme'); ?>
            </div>
            <span class="btnReadMore"><a href="<?php the_field('link_thuong_hiệu'); ?>"><?php _e("<!--:en-->Read More<!--:--><!--:vi-->Tìm hiểu thêm<!--:-->"); ?></a></span>
        </div>
    </div>
    <!--end /#gioiThieu-->

    <div id="khachHang">
        <div class="container wow fadeInUp">
            <h3><?php the_field('tieu_dề_khach_hang'); ?></h3>
            <span class="khIntro">
                <?php the_field('mo_tả_ngắn_khach_hang'); ?>
            </span>
            <span class="btnReadMore"><a href="<?php the_field('link_khach_hang'); ?>"><?php _e("<!--:en-->Read More<!--:--><!--:vi-->Tìm hiểu thêm<!--:-->"); ?></a></span>
        </div>
    </div>
    <!--end /#khachHang-->

    <div id="kyThuat">
        <div class="container wow fadeInUp">
            <div class="row">
                <div class="col-sm-6">
                    <img src="<?php the_field('hinh_ảnh_cấu_tạo_san_gỗ'); ?>" alt="<?php the_field('tieu_dề_cấu_tạo_san_gỗ'); ?>">
                </div>
                <div class="col-sm-6">
                    <h2><?php the_field('tieu_dề_cấu_tạo_san_gỗ'); ?></h2>
                    <span class="ktTxt">
                        <?php the_field('mo_tả_ngắn_cấu_tạo_san_gỗ'); ?>
                    </span>
                    <span class="btnReadMore"><a href="<?php the_field('link_cấu_tạo_san_gỗ'); ?>"><?php _e("<!--:en-->Read More<!--:--><!--:vi-->Tìm hiểu thêm<!--:-->"); ?></a></span>
                </div>
            </div>
        </div>
    </div>
    <!--end /#kyThuat-->

    <div id="sanPham">
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-sm-3">
                    <h2><?php the_field('tieu_dề_sản_phẩm'); ?></h2>
                    <span class="spTxt">
                        <?php the_field('mo_tả_ngắn_sản_phẩm'); ?>
                    </span>
                    <span class="btnReadMore"><a href="<?php bloginfo('home'); ?>/san-pham/"><?php _e("<!--:en-->Read More<!--:--><!--:vi-->Tìm hiểu thêm<!--:-->"); ?></a></span>
                </div>
                <div class="col-sm-9">
                    <span class="countItem"></span>
                    <div class="spSlick">
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img00.jpg);"></span>
                            <h3>Quis ipsum suspendisse </h3>
                            <span class="spITxt">Lorem ipsum dolor </span>
                        </div>

                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img01.jpg);"></span>
                            <h3>Quis ipsum suspendisse </h3>
                            <span class="spITxt">Lorem ipsum dolor </span>
                        </div>
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img02.jpg);"></span>
                            <h3>Quis ipsum suspendisse </h3>
                            <span class="spITxt">Lorem ipsum dolor </span>
                        </div>
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img00.jpg);"></span>
                            <h3>Quis ipsum suspendisse </h3>
                            <span class="spITxt">Lorem ipsum dolor </span>
                        </div>
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img01.jpg);"></span>
                            <h3>Quis ipsum suspendisse </h3>
                            <span class="spITxt">Lorem ipsum dolor </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end /#sanPham-->

    <div id="duAn">
        <div class="grid-1107 wow fadeInUp">
            <span class="daTT00"><?php _e("<!--:en-->Projects have been implemented<!--:--><!--:vi-->Các dự án đã thực hiện<!--:-->"); ?></span>
            <div class="duSlick">
                <?php 
                $args=array(
                'post_type' => 'du-an',  // slug luc dang ky trong function
                'orderby' => 'ID', 
                'order'   => 'DESC', 
                'posts_per_page'=>6);
                $query = new WP_Query( $args); 
                ?>
                <?php if ( $query->have_posts() ) : ?>
                <?php while ( $query->have_posts() ) : $query->the_post();?>
                    <div class="duItem" style="background-image: url('<?php if ( has_post_thumbnail() ){ the_post_thumbnail_url(); } else { echo get_stylesheet_directory_uri().'/images/ho-img07.jpg'; } ?>">
                        <a href="<?php the_permalink() ?>"></a>
                        <span><?php _e("<!--:en-->Read More<!--:--><!--:vi-->Tìm hiểu thêm<!--:-->"); ?></span>
                    </div>
                <?php endwhile; ?>
                <?php  wp_reset_query(); ?>
                <?php  endif; ?>
            </div>
        </div>
    </div>
    <!--end /#duAn-->

    <div id="doiTac">
        <div class="grid-1107 wow fadeInUp">
            <span class="dtTt00"><?php the_field('tieu_dề_dối_tac_khach_hang'); ?></span>
            <div class="dtSlick">
                <?php if( have_rows('logo_dối_tac') ): while ( have_rows('logo_dối_tac')) : the_row(); ?> 
                    <div class="dtItem">
                        <img src="<?php the_sub_field('hinh_ảnh_logo_dối_tac'); ?>" alt="<?php the_sub_field('ten_dối_tac'); ?>">
                    </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
    <!--end /#doiTac-->

</main>
<!-- End main -->
<?php get_footer(); ?>
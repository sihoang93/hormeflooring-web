<?php

function admin_style()
{
    wp_enqueue_style('admin-styles', get_template_directory_uri() . '/inc/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');
function my_login_stylesheet()
{
    wp_enqueue_style('admin-styles', get_template_directory_uri() . '/inc/admin.css');
}
add_action('login_enqueue_scripts', 'my_login_stylesheet');
include('inc/duplicate-post.php');
include('inc/thumbnails.php');
//include('inc/theme-options.php');
//remove_filter ( 'the_content' , 'wpautop' ) ;
include('inc/theme-options.php');
include('inc/widget.php');
include('inc/post-type-product.php');
include('inc/cpt-duan.php');
include('inc/cpt-hoatdong.php');
add_theme_support('menus');
add_action('init', 'register_my_menus');

function register_my_menus()
{
    register_nav_menus(
        array(
            'main-nav' => 'Main Navigation',
            'bot-nav' => 'Footer Navigation'
        )
    );
}

add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

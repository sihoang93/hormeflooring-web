<!DOCTYPE html>
<html itemscope="" itemtype="http://schema.org/WebPage" lang="<?php bloginfo('language'); ?>">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo esc_attr(get_bloginfo('name', 'display')); ?></title>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/images/favicon.png" type="image/x-icon">
    <link href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/css/font-awesome.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo get_template_directory_uri() ?>/css/icofont.css" type="text/css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/css/animate.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo get_template_directory_uri() ?>/css/slick.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo get_template_directory_uri() ?>/css/hover.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo get_template_directory_uri() ?>/css/reset.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo get_template_directory_uri() ?>/css/magnific-popup.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo get_template_directory_uri() ?>/style.css" type="text/css" rel="stylesheet" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <!-- Header -->
    <header>
        <div class="wrapHeader">
            <div class="container">
                <div class="row">
                    <div class="logo">
                        <a href="<?php bloginfo('home'); ?>">
                            <img src="<?php the_field('logo_header', 'options'); ?>" alt="<?php bloginfo('name'); ?>" class="imgNoSc">
                            <img src="<?php the_field('logo_header_on_scroll', 'options'); ?>" alt="<?php bloginfo('name'); ?>" class="imgSc">
                        </a>
                    </div>
                    <div class="navHe">
                        <div class="row-nav">
                            <nav class="main-nav">
                                <?php
                                $defaults = array(
                                    'theme_location'  => 'main-nav', //Slug đăng ký menu
                                    'menu'            => '',
                                    'container'       => '', //Định nghĩa đối tượng bao bọc menu
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => '', //Tên css sử dụng cho phần tử menu
                                    'menu_id'         => '', //Tương tự như menu_class
                                    'echo'            => true,
                                    'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                    'walker'          => ''
                                    ); 
                                wp_nav_menu( $defaults );
                                ?>
                            </nav>
                            <ul class="lang main-option">
                                <li><a href="javascript:void(0)" class="smooth btn-search"><i class="fa fa-search"
                                            aria-hidden="true"></i></a></li>
                                <li>
                                    <?php if ( is_active_sidebar( 'sidebar01' ) ) : ?>
                                        <?php dynamic_sidebar( 'sidebar01' ); ?>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </div>
                        <form action="" class="form-search"><input type="search" placeholder="Tìm kiếm">
                        </form>
                        <button class="menu-btn nav-open d-xl-none" type="button"><i></i></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Menu -->


    </header>
    <div class="m-nav d-xl-none">
        <button class="menu-btn act nav-close" type="button"><i></i></button>
        <div class="nav-ct">
        <?php if ( is_active_sidebar( 'sidebar02' ) ) : ?>
            <?php dynamic_sidebar( 'sidebar02' ); ?>
        <?php endif; ?>
        </div>
    </div>
    <!-- End header -->
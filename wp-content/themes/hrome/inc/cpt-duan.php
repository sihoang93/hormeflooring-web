<?php
add_action('init', 'register_my_duan_cpts');
function register_my_duan_cpts()
{
    $labels = array(
        "name" => "Dự án",
        "singular_name" => "Dự án",
        "menu_name" => "Dự án",
        "all_items" => "Tất cả Dự án",
        "add_new" => "Thêm mới",
        "add_new_item" => "Thêm mới Dự án",
        "edit" => "Sửa",
        "edit_item" => "Sửa Dự án",
        "new_item" => "Dự án mới",
        "view" => "Hiển thị",
        "view_item" => "Hiển thị Dự án",
        "search_items" => "Tìm kiếm Dự án",
        "not_found" => "Không tìm thấy Dự án",
        "not_found_in_trash" => "Không tìm thấy Dự án trong thùng rác",
        "parent" => "Dự án cha"
    );
    $args   = array(
        "labels" => $labels,
        "description" => "Custom post type Dự án",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array(
            "slug" => "du-an",
            "with_front" => true
		),
		// 'show_in_rest' => true,
        "supports" => array(
            'title',
            'editor',
            'thumbnail',
            'comments',
            'custom-fields'
        ),
        "query_var" => true,
        "menu_position" => 5,
        "menu_icon" => "dashicons-book-alt",
        "taxonomies" => array(
            "tu-khoa",
            "danh-muc-du-an"
        )
    );
    register_post_type("du-an", $args);
}
// End of register_my_duan_cpts()

add_action('init', 'register_my_duan_taxes');
function register_my_duan_taxes()
{
    $labels = array(
        "name" => "Danh mục Dự án",
        "label" => "Danh mục Dự án",
        "menu_name" => "Danh mục",
        "all_items" => "Tất cả danh mục",
        "edit_item" => "Chỉnh sửa danh mục",
        "view_item" => "Hiển thị danh mục",
        "update_item" => "Cập nhật tên danh mục",
        "add_new_item" => "Thêm mới danh mục",
        "new_item_name" => "Tên danh mục mới",
        "parent_item" => "Danh mục cha",
        "parent_item_colon" => "Danh mục cha:",
        "search_items" => "Tìm kiếm danh mục",
        "popular_items" => "Danh mục phổ biến",
        "separate_items_with_commas" => "Ngăn cách chuyên mục bằng dấu phẩy",
        "add_or_remove_items" => "Thêm hoặc xóa danh mục",
        "choose_from_most_used" => "Chọn danh mục được sử dụng nhiều nhất",
        "not_found" => "Không tìm thấy danh mục"
    );
    $args   = array(
        "labels" => $labels,
        "hierarchical" => true,
        "label" => "Danh mục Dự án",
        "show_ui" => true,
        "query_var" => true,
        "rewrite" => array(
            'slug' => 'danh-muc-du-an',
            'with_front' => true
        ),
        "show_admin_column" => true
    );
    register_taxonomy("danh-muc-du-an", array(
        "du-an"
    ), $args);
    // End Register Taxonomies
    
    $labels = array(
        "name" => "Từ khóa Dự án",
        "label" => "Từ khóa Dự án",
        "menu_name" => "Từ khóa",
        "all_items" => "Tất cả Từ khóa",
        "edit_item" => "Chỉnh sửa Từ khóa",
        "view_item" => "Hiển thị Từ khóa",
        "update_item" => "Cập nhật tên Từ khóa",
        "add_new_item" => "Thêm Từ khóa mới",
        "new_item_name" => "Tên Từ khóa mới",
        "parent_item" => "Từ khóa cha",
        "parent_item_colon" => "Từ khóa cha:",
        "search_items" => "Tìm kiếm Từ khóa",
        "popular_items" => "Từ khóa phổ biến",
        "separate_items_with_commas" => "Ngăn cách Từ khóa bằng dấu phẩy",
        "add_or_remove_items" => "Thêm hoặc xóa Từ khóa",
        "choose_from_most_used" => "Chọn Từ khóa được sử dụng nhiều nhất",
        "not_found" => "Không tìm thấy Từ khóa"
    );
    $args   = array(
        "labels" => $labels,
        "hierarchical" => false,
        "label" => "Từ khóa Dự án",
        "show_ui" => true,
        "query_var" => true,
        "rewrite" => array(
            'slug' => 'tu-khoa',
            'with_front' => true
        ),
        "show_admin_column" => true
    );
    register_taxonomy("tu-khoa", array(
        "du-an"
    ), $args);
    // End Register Taxonomies
}
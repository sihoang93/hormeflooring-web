<?php
add_action('init', 'register_my_hoat_cpts');
function register_my_hoat_cpts()
{
    $labels = array(
        "name" => "Hoạt động",
        "singular_name" => "Hoạt động",
        "menu_name" => "Hoạt động",
        "all_items" => "Tất cả Hoạt động",
        "add_new" => "Thêm mới",
        "add_new_item" => "Thêm mới Hoạt động",
        "edit" => "Sửa",
        "edit_item" => "Sửa Hoạt động",
        "new_item" => "Hoạt động mới",
        "view" => "Hiển thị",
        "view_item" => "Hiển thị Hoạt động",
        "search_items" => "Tìm kiếm Hoạt động",
        "not_found" => "Không tìm thấy Hoạt động",
        "not_found_in_trash" => "Không tìm thấy Hoạt động trong thùng rác",
        "parent" => "Hoạt động cha"
    );
    $args   = array(
        "labels" => $labels,
        "description" => "Custom post type Hoạt động",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array(
            "slug" => "hoat-dong",
            "with_front" => true
		),
		// 'show_in_rest' => true,
        "supports" => array(
            'title',
            'editor',
            'thumbnail',
            'comments',
            'custom-fields'
        ),
        "query_var" => true,
        "menu_position" => 6,
        "menu_icon" => "dashicons-universal-access",
        "taxonomies" => array(
            "tu-khoa",
            "danh-muc-hoat-dong"
        )
    );
    register_post_type("hoat-dong", $args);
}
// End of register_my_hoat_cpts()

add_action('init', 'register_my_hoat_taxes');
function register_my_hoat_taxes()
{
    $labels = array(
        "name" => "Danh mục Hoạt động",
        "label" => "Danh mục Hoạt động",
        "menu_name" => "Danh mục",
        "all_items" => "Tất cả danh mục",
        "edit_item" => "Chỉnh sửa danh mục",
        "view_item" => "Hiển thị danh mục",
        "update_item" => "Cập nhật tên danh mục",
        "add_new_item" => "Thêm mới danh mục",
        "new_item_name" => "Tên danh mục mới",
        "parent_item" => "Danh mục cha",
        "parent_item_colon" => "Danh mục cha:",
        "search_items" => "Tìm kiếm danh mục",
        "popular_items" => "Danh mục phổ biến",
        "separate_items_with_commas" => "Ngăn cách chuyên mục bằng dấu phẩy",
        "add_or_remove_items" => "Thêm hoặc xóa danh mục",
        "choose_from_most_used" => "Chọn danh mục được sử dụng nhiều nhất",
        "not_found" => "Không tìm thấy danh mục"
    );
    $args   = array(
        "labels" => $labels,
        "hierarchical" => true,
        "label" => "Danh mục Hoạt động",
        "show_ui" => true,
        "query_var" => true,
        "rewrite" => array(
            'slug' => 'danh-muc-hoat-dong',
            'with_front' => true
        ),
        "show_admin_column" => true
    );
    register_taxonomy("danh-muc-hoat-dong", array(
        "hoat-dong"
    ), $args);
    // End Register Taxonomies
    
    $labels = array(
        "name" => "Từ khóa Hoạt động",
        "label" => "Từ khóa Hoạt động",
        "menu_name" => "Từ khóa",
        "all_items" => "Tất cả Từ khóa",
        "edit_item" => "Chỉnh sửa Từ khóa",
        "view_item" => "Hiển thị Từ khóa",
        "update_item" => "Cập nhật tên Từ khóa",
        "add_new_item" => "Thêm Từ khóa mới",
        "new_item_name" => "Tên Từ khóa mới",
        "parent_item" => "Từ khóa cha",
        "parent_item_colon" => "Từ khóa cha:",
        "search_items" => "Tìm kiếm Từ khóa",
        "popular_items" => "Từ khóa phổ biến",
        "separate_items_with_commas" => "Ngăn cách Từ khóa bằng dấu phẩy",
        "add_or_remove_items" => "Thêm hoặc xóa Từ khóa",
        "choose_from_most_used" => "Chọn Từ khóa được sử dụng nhiều nhất",
        "not_found" => "Không tìm thấy Từ khóa"
    );
    $args   = array(
        "labels" => $labels,
        "hierarchical" => false,
        "label" => "Từ khóa Hoạt động",
        "show_ui" => true,
        "query_var" => true,
        "rewrite" => array(
            'slug' => 'tu-khoa',
            'with_front' => true
        ),
        "show_admin_column" => true
    );
    register_taxonomy("tu-khoa", array(
        "hoat-dong"
    ), $args);
    // End Register Taxonomies
}
<div class="comPage">
    <div class="bannerPage ow" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/images/da-img01.jpg'; ?>');"></div>
    <div class="breadKum">
        <div class="grid-1175">
            <ul>    
                <li><a href="<?php bloginfo('home'); ?>"><?php _e("<!--:en-->Home<!--:--><!--:vi-->Trang chủ<!--:-->"); ?></a></li>
                <li><a href="<?php bloginfo('home'); ?>/danh-muc-du-an/"><?php _e("<!--:en-->Project<!--:--><!--:vi-->Dự án<!--:-->"); ?></a></li>
                <li class="active"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="comPage">
    <div class="bannerPage ow" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/images/hdctn-img00.jpg'; ?>');"></div>
    <div class="breadKum">
        <div class="grid-1175">
            <ul>    
                <li><a href="<?php bloginfo('home'); ?>"><?php _e("<!--:en-->Home<!--:--><!--:vi-->Trang chủ<!--:-->"); ?></a></li>
                <li><a href="<?php bloginfo('home'); ?>/hoat-dong-cua-chung-toi/"><?php _e("<!--:en-->Our activities<!--:--><!--:vi-->Hoạt động của chúng tôi<!--:-->"); ?></a></li>
                <li class="active"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
            </ul>
        </div>
    </div>
</div>
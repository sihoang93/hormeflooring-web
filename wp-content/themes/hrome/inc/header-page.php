<div class="comPage">
    <div class="bannerPage ow" style="background-image: url('<?php if ( has_post_thumbnail() ){ the_post_thumbnail_url(); } else { echo get_stylesheet_directory_uri().'/images/ad-img00.jpg'; } ?>');"></div>
    <div class="breadKum">
        <div class="grid-1175">
            <ul>    
                <li><a href="<?php bloginfo('home'); ?>"><?php _e("<!--:en-->Home<!--:--><!--:vi-->Trang chủ<!--:-->"); ?></a></li>
                <li class="active"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
            </ul>
        </div>
    </div>
</div>
<?php 
// view thumbnails
add_theme_support( 'post-thumbnails' ); 

// thêm cột để chứa anhrcho cả post và page
add_filter('manage_posts_columns', 'tcb_add_post_thumbnail_column', 5);
  
// hàm để thêm cột
function tcb_add_post_thumbnail_column($cols){
  $cols['tcb_post_thumb'] = __('Image');
  return $cols;
}
  
// gọi lại hàm để tạo ảnh
add_action('manage_posts_custom_column', 'tcb_display_post_thumbnail_column', 5, 2);
  
// hàm thêm ảnh vào trong list post và list page
function tcb_display_post_thumbnail_column($col, $id){
  switch($col){
   case 'tcb_post_thumb':
      if( function_exists('the_post_thumbnail') )
       echo the_post_thumbnail( array(90,90) );
     else
       echo 'Not supported in theme';
    break;
  }
}
<?php /* Template Name: Cơ hội nghề nghiệp */ ?>
<?php get_header();?>
<?php get_template_part( 'inc/header', 'page' ); ?>
<!-- Main -->
<main>
    <section id="coHoi">
        <div class="grid-1175">
            <div class="chContent01 wow fadeInUp">
                <div class="grid-915">
                    <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>
                    <div class="chTxt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.<br>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.<br><br>

                        Risus commodo viverra maecenas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum.
                    </div>
                </div>
                <ul class="chBanner">
                    <li><img src="<?php echo get_template_directory_uri() ?>/images/ch-img01.jpg" alt="<?php the_title(); ?>"></li>
                    <li><img src="<?php echo get_template_directory_uri() ?>/images/ch-img02.jpg" alt="<?php the_title(); ?>"></li>
                </ul>
            </div>
            <div class="chContent02 wow fadeInUp">
                <h2>Vị trí tuyển dụng</h2>
                <div class="ch02Rows">
                    <div class="ch02Item">
                        <div class="ch02IChild">
                            <h3>Phòng ban</h3>
                            <div class="ch02FullTxt">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                <ul class="chList inline-popups">
                                    <li><a href="#test-popup" data-effect="mfp-zoom-out">Vị trí tuyển dụng</a></li>
                                    <li><a href="#test-popup" data-effect="mfp-zoom-out">Vị trí tuyển dụng</a></li>
                                    <li><a href="#test-popup" data-effect="mfp-zoom-out">Vị trí tuyển dụng</a></li>
                                    <li><a href="#test-popup" data-effect="mfp-zoom-out">Vị trí tuyển dụng</a></li>
                                </ul>
                                <!-- Popup itself -->
                                <div id="test-popup" class="white-popup mfp-with-anim mfp-hide">
                                    <h3>Vị trí tuyển dụng</h3>
                                    <form action="" method="post" class="fromRecurit">
                                        <ul>
                                            <li>
                                                <span>NAME(*):</span>
                                                <input type="text" name="name" class="form-control" required>
                                            </li>
                                            <li>
                                                <span>TEL(*):</span>
                                                <input type="tel" name="" class="form-control" required>
                                            </li>
                                            <li>
                                                <span>EMAIL(*):</span>
                                                <input type="email" name="" class="form-control" required>
                                            </li>
                                            <li>
                                                <span>ADDRESS:</span>
                                                <input type="text" name="" class="form-control">
                                            </li>
                                            <li>
                                                <span>NOTE:</span>
                                                <input type="text" name="" class="form-control">
                                            </li>
                                            <li>
                                                <span>UPLOAD CV:</span>
                                                <input type="file" name="" class="form-control">
                                                <span class="note">YOU CAN ONLY ATTACH .DOC, .DOCX, JPG, PNG AND .PDF FILES THAT ARE LESS THAN 2048KB IN SIZE</span>
                                            </li>
                                            <li>
                                                <input type="submit" value="Apply">
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ch02Item">
                        <div class="ch02IChild">
                            <h3>consectetur adipiscing elit</h3>
                            <div class="ch02FullTxt">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                <ul class="chList">
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="ch02IChild">
                            <h3>Risus commodo </h3>
                            <div class="ch02FullTxt">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                <ul class="chList">
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="ch02IChild">
                            <h3>Lorem ipsum dolor</h3>
                            <div class="ch02FullTxt">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                <ul class="chList">
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="ch02IChild">
                            <h3>eiusmod tempor incididunt</h3>
                            <div class="ch02FullTxt">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                <ul class="chList">
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="ch02IChild">
                            <h3>Lorem ipsum dolor</h3>
                            <div class="ch02FullTxt">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                <ul class="chList">
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="ch02IChild">
                            <h3>accumsan lacus</h3>
                            <div class="ch02FullTxt">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                <ul class="chList">
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                    <li><a href="">Vị trí tuyển dụng</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- End main -->
<?php get_footer(); ?>
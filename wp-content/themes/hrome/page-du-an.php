<?php /* Template Name: Dự án */ ?>
<?php get_header();?>
<?php get_template_part( 'inc/header', 'page' ); ?>
<!-- Main -->
<main>

<section id="duAnPage">
    <div class="grid-1175">
        
        <ul class="daNav wow fadeInUp">
            <?php
                $args = array(
                    'taxonomy' => 'danh-muc-du-an',
                    'orderby' => 'Id',
                    'order'   => 'DESC'
                );
                $cats = get_categories($args);
                foreach($cats as $cat) {
                ?>
                    <li><a href="<?php echo get_category_link( $cat->term_id ) ?>"><?php echo $cat->name; ?></a></li>
            <?php } ?>
        </ul>
        <div class="daInfo">
        <?php $i = 1; $j = 1;
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
                $args = array(
                'post_type' => 'du-an', // Gọi danh sách kiểu post type là post
                'orderby' => 'date',  // Sắp xếp theo ngày tháng
                'order'   => 'DESC', 
                'posts_per_page'=>6, // Số bài viết hiển thị
                'paged' => $paged
                ); query_posts($args);?>
                <?php if ( have_posts() ) : ?>
            <div class="daRows wow fadeInUp">
                <?php while ( have_posts() ) : the_post();?>
                    <div class="<?php if($j%2 != 0) { echo 'daLeft'; } else {echo 'daRight'; } ?> ">
                        <a href="<?php the_permalink() ?>"></a>
                        <span class="daImg" style="background-image: url('<?php if ( has_post_thumbnail() ){ the_post_thumbnail_url(); } else { echo get_stylesheet_directory_uri().'/images/da-img02.jpg'; } ?>');">
                            <span class="daTxt"><?php _e("<!--:en-->Read More<!--:--><!--:vi-->Tìm hiểu thêm<!--:-->"); ?></span>
                        </span>
                        <div class="daInfoTxt ow">
                            <h3><?php the_title(); ?></h3>
                            <span><?php echo mb_substr(get_the_excerpt(), 0, 150,'UTF-8').'...'; ?></span>
                        </div>
                    </div>
                <?php if(  $i%2 ==0  ) { 
                    echo '</div><div class="daRows wow fadeInUp">';}  ?>
                <?php  $i++; $j++; endwhile; ?>
            </div>
            <div class="wrap-pagi">
                <?php wp_pagenavi(); /* Vị trí hiển thị phân trang */?> 
            </div>
                <?php  wp_reset_query(); ?>
            <?php  endif; ?>
        </div>
    </div>
</section>

</main>
<!-- End main -->
<?php get_footer(); ?>
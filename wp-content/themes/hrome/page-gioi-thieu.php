<?php /* Template Name: Giới thiệu */ ?>
<?php get_header();?>
<?php get_template_part( 'inc/header', 'page' ); ?>

<!-- Main -->
<main>

    <section id="gioiThieuPage">
        <div class="grid-1175">
            <div class="gtRows01 ow wow fadeInUp">
                <div class="gt01Left">
                    <h3 class="ttSp"><?php the_field('tieu_dề_hoạt_dộng_của_chung_toi'); ?></h3>
                    <img src="<?php the_field('hinh_ảnh_hoạt_dộng'); ?>" alt="<?php the_field('tieu_dề_hoạt_dộng_của_chung_toi'); ?>">
                    <div class="gt365">
                        <span>
                            <?php the_field('mo_tả_ngắn_hoạt_dộng_của_chung_toi'); ?>
                        </span>
                        <h3><?php the_field('tieu_dề_hoạt_dộng_của_chung_toi'); ?></h3>
                    </div>
                </div>
                <div class="gt01Right">
                    <h2><?php the_field('tieu_dề_giới_thiệu_thuong_hiệu'); ?></h2>
                    <span class="txt">
                        <?php the_field('mo_tả_ngắn_thuong_hiệu'); ?>
                    </span>
                    <span class="img"><img src="<?php the_field('hinh_ảnh_giới_thiệu_thuong_hiệu'); ?>" alt="<?php the_field('tieu_dề_giới_thiệu_thuong_hiệu'); ?>"></span>
                </div>
            </div>
            <div class="gtRows02 ow wow fadeInUp">
                <div class="gt02R">
                    <div class="gt02Left">
                        <h2><?php the_field('tieu_dề_tầm_nhin'); ?></h2>
                    </div>
                    <div class="gt02Right">
                        <span class="img"><img src="<?php the_field('hinh_ảnh_tầm_nhin'); ?>" alt="<?php the_field('tieu_dề_tầm_nhin'); ?>"></span>
                        <span class="infoMax"><?php the_field('mo_tả_tầm_nhin_01'); ?></span>
                        <span class="infoMax800"><?php the_field('mo_tả_tầm_nhin_02'); ?></span>
                        <span class="infoMax"><?php the_field('mo_tả_tầm_nhin_03'); ?><br><br></span>
                        <span class="infoMax"><?php the_field('mo_tả_tầm_nhin_04'); ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<!-- End main -->
<?php get_footer(); ?>
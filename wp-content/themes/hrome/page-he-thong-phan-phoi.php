<?php /* Template Name: Hệ thống phân phối */ ?>
<?php get_header();?>
<?php get_template_part( 'inc/header', 'page' ); ?>
<!-- Main -->
<main>
    <section id="heThongPhanPhoi">
        <div class="grid-1175">
            <ul class="htSel">
                <li>
                    <select name="" id="">
                        <option value="">Chọn tỉnh/Thành phố</option>
                        <option value="">Bình Định</option>
                    </select>
                </li>
                <li>
                    <select name="" id="">
                        <option value="">Chọn tỉnh/Thành phố</option>
                        <option value="">Bình Định</option>
                    </select>
                </li>
                <li>
                    <select name="" id="">
                        <option value="">Chọn tỉnh/Thành phố</option>
                        <option value="">Bình Định</option>
                    </select>
                </li>
                <li>
                    <button type="submit">TÌM KIẾM</button>
                </li>
            </ul>
            <div class="htContent">
                <div class="htLeft">
                    <div class="htLeftS">
                        <ul>
                            <li>
                                <span class="htName"><a href="">Công Ty TNHH TM TT NT Thành Phước</a></span>
                                <span class="htAddress"><i class="fa fa-map-marker" aria-hidden="true"></i> 90/217B Trường Chinh, P12, Tân Bình, TP.HCM</span>
                                <span class="htTel"><a href="tel:0908130280"><i class="fa fa-phone" aria-hidden="true"></i> 0908130280</a></span>
                                <span class="htBtn"><a href="">Xem chi tiết <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                            </li>
                            <li>
                                <span class="htName"><a href="">Công Ty TNHH TM DV Thiên Minh</a></span>
                                <span class="htAddress"><i class="fa fa-map-marker" aria-hidden="true"></i> 263 Tô Hiến Thành, Phường 13, Quận 10, TP.HCM</span>
                                <span class="htTel"><a href="tel:0908441019"><i class="fa fa-phone" aria-hidden="true"></i> 0908441019</a></span>
                                <span class="htBtn"><a href="">Xem chi tiết <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                            </li>
                            <li>
                                <span class="htName"><a href="">Công Ty TNHH MTV XD & TTNT Cường Thịnh</a></span>
                                <span class="htAddress"><i class="fa fa-map-marker" aria-hidden="true"></i> 153 Bùi Đình Túy, P.24, Q. Bình Thạnh, TP.HCM</span>
                                <span class="htTel"><a href="tel:0908352068"><i class="fa fa-phone" aria-hidden="true"></i> 0908352068</a></span>
                                <span class="htBtn"><a href="">Xem chi tiết <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                            </li>
                            <li>
                                <span class="htName"><a href="">Công Ty Song Thắng</a></span>
                                <span class="htAddress"><i class="fa fa-map-marker" aria-hidden="true"></i> Số 29, Đường D4, Khu KDC Him Lam, P. Tân Hưng, Q.7, TP.HCM</span>
                                <span class="htTel"><a href="tel:0888883458"><i class="fa fa-phone" aria-hidden="true"></i> 0888883458</a></span>
                                <span class="htBtn"><a href="">Xem chi tiết <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                            </li>
                            <li>
                                <span class="htName"><a href="">Công Ty TNHH Đầu Tư HCM</a></span>
                                <span class="htAddress"><i class="fa fa-map-marker" aria-hidden="true"></i> 60 Gò Cát, Phường Phú Hữu, Q9, TP.HCM</span>
                                <span class="htTel"><a href="tel:0911946094"><i class="fa fa-phone" aria-hidden="true"></i> 0911946094</a></span>
                                <span class="htBtn"><a href="">Xem chi tiết <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                            </li>
                            <li>
                                <span class="htName"><a href="">Công Ty TNHH TM TT NT Thành Phước</a></span>
                                <span class="htAddress"><i class="fa fa-map-marker" aria-hidden="true"></i> 90/217B Trường Chinh, P12, Tân Bình, TP.HCM</span>
                                <span class="htTel"><a href="tel:0908130280"><i class="fa fa-phone" aria-hidden="true"></i> 0908130280</a></span>
                                <span class="htBtn"><a href="">Xem chi tiết <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="htRight">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.1945274800905!2d106.64438231528405!3d10.796408261775577!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175294c0bbb2387%3A0xfa2e82d8d8d78fac!2zOTAsIDIxN2IgVHLGsOG7nW5nIENoaW5oLCBQaMaw4budbmcgMTIsIFTDom4gQsOsbmgsIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1587999139862!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- End main -->
<?php get_footer(); ?>
<?php /* Template Name: Sản phẩm */ ?>
<?php get_header();?>
<?php get_template_part( 'inc/header', 'page' ); ?>
<!-- Main -->
<main>
    <section id="spPage">
        <div class="spContent01">
            <div class="grid-1175">
                <div class="spC01Rows">
                    <div class="spCo01Left wow fadeInUp">
                        <div class="spCo01Item">
                            <h3>Bộ sưu tập</h3>
                            <ul>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Sàn gỗ Acacia kỹ thuật (130)</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Sàn gỗ Sồi kỹ thuật (90)</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Sàn gỗ Walnut kỹ thuật (54)</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Phụ kiện Len HF (47)</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="spCo01Item">
                            <h3>Màu sắc</h3>
                            <ul>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark"><strong style="background-color: #757575;"></strong>Grey (130)</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark"><strong style="background-color: #d1b491;"></strong>Beige (90)</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark"><strong style="background-color: #3c3c3b;"></strong>Black (54)</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark"><strong style="background-color: #4f2c17;"></strong>Brown (47)</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark"><strong style="background-color: #4f2c17;"></strong>Brown (47)</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="spCo01Item">
                            <h3>Chủng loại</h3>
                            <ul>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Eiusmod tempor</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Quis ipsum</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Incididunt</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Pendisse ultrices</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="spCo01Item">
                            <h3>Bề mặt</h3>
                            <ul>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Eiusmod tempor</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Quis ipsum</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Incididunt</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Pendisse ultrices</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="spCo01Item">
                            <h3>Bề mặt</h3>
                            <ul>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Eiusmod tempor</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Quis ipsum</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Incididunt</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container">
                                        <input type="checkbox" name="">
                                        <span class="checkmark">Pendisse ultrices</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="spCo01Right wow fadeInUp">
                
                        <div class="slick-wrapper">
                            <div id="slick1">
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img08.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF902</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img09.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF903</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img10.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF904</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img11.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF905</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img12.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF905</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img13.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF907</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img14.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF908</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img15.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF909</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="slide-item">
                                    <div class="boxes">
                                        <div class="item-product">
                                            <div class="hot-news">New</div>
                                            <div class="thumbnail-product">
                                                <a href="#" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img03.jpg);"></a>
                                            </div>
                                            <h2>Sồi kỹ thuật EHF901</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                                            <a href="#" class="more-infor">Tìm hiểu thêm</a>
                                        </div>
                                    </div>
                                    </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="spContent02">
            <div class="grid-1175 wow fadeInUp">
                <h2>CHƯƠNG TRÌNH KHUYẾN MÃI</h2>
                <div class="spWrap02">
                    <div class="spSlick4">
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img04.jpg);"></span>
                            <div class="item-info">
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="spITxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</span>
                                <span class="spIDate">10 tháng 3, 2020</span>
                            </div>
                        </div>

                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img05.jpg);"></span>
                            <div class="item-info">
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="spITxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</span>
                                <span class="spIDate">10 tháng 3, 2020</span>
                            </div>
                        </div>
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img06.jpg);"></span>
                            <div class="item-info">
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="spITxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</span>
                                <span class="spIDate">10 tháng 3, 2020</span>
                            </div>
                        </div>
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img07.jpg);"></span>
                            <div class="item-info">
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="spITxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</span>
                                <span class="spIDate">10 tháng 3, 2020</span>
                            </div>
                        </div>
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img04.jpg);"></span>
                            <div class="item-info">
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="spITxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</span>
                                <span class="spIDate">10 tháng 3, 2020</span>
                            </div>
                        </div>

                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img05.jpg);"></span>
                            <div class="item-info">
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="spITxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</span>
                                <span class="spIDate">10 tháng 3, 2020</span>
                            </div>
                        </div>
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img06.jpg);"></span>
                            <div class="item-info">
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="spITxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</span>
                                <span class="spIDate">10 tháng 3, 2020</span>
                            </div>
                        </div>
                        <div class="spItem">
                            <span class="spImg" style="background-image: url(<?php echo get_template_directory_uri() ?>/images/sp-img07.jpg);"></span>
                            <div class="item-info">
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="spITxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</span>
                                <span class="spIDate">10 tháng 3, 2020</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- End main -->
<?php get_footer(); ?>
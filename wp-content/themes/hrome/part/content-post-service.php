
      
                      
                        	<div class="service-item">
                            	<div class="service-image"><a href="<?php the_permalink() ?>" class="hv-o"><?php if ( has_post_thumbnail() ) { ?>
           				 <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
            				<?php } else { ?>
            			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/sample.png" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
            		<?php } ?>
            		</a></div>
                    				<div class="service-text row">
                            			<div class="service-name"><h3> <?php the_title(); ?></h3></div>
                                		<div class="service-des"><?php the_excerpt();?>	</div>
                                	</div>
                                <div class="service-btn-detail row">
                                	<a href="<?php the_permalink() ?>" class="hv-o"><img src="<?php echo get_template_directory_uri() ?>/assets/images/btn-detail.png" alt="detail"></a>
                                </div>
                            </div>
				
 
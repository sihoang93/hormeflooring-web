<?php get_header();?>
<!-- Posts and Widgets -->
<section class="content clearfix mgt80">
    <div class="container">
        <div class="row">
            <!-- Posts -->
            <div class="col-md-8">
                <span class="sp-01 sp-co">Từ khoá :</span><span class="sp-02 sp-co"><?php echo  get_search_query();  ?></span>
                <div class="posts">
                
                  <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                          <?php if ( 'post' == get_post_type() ) : ?> 
                              <?php get_template_part( 'content', 'result' ); ?>  
                          <?php endif; ?>
                      <?php  endwhile; ?>
                  <?php  else:  get_template_part( 'content', 'none' );   endif; ?>
                  <?php  wp_reset_query(); ?>
                    </div>
                </div>
            <!-- Sidebar - Widgets -->
            <div class="col-md-4">
              <?php get_template_part('inc/sidebar'); ?>
            </div>
        </div>
    </div>
</section><!-- posts-widgets-END -->

<?php get_footer(); ?>

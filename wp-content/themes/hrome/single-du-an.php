<?php get_header();?>
<?php get_template_part( 'inc/header', 'duan' ); ?>
<!-- Main -->
<main>
    <section id="duAnChiTiet">
        <div class="daCT01 ow wow fadeInUp">
            <?php if ( have_posts() ) :  ?>
            <?php while ( have_posts() ) : the_post(); ?>
                    <!-- Content -->
                        <div class="grid-1175">
                            <h2><?php the_title(); ?></h2>
                            <div class="daCtSlide">
                                <ul id="slider01">
                                    <?php if( have_rows('slide_dự_an') ): while ( have_rows('slide_dự_an')) : the_row(); ?>
                                        <li class="slide-item"><img data-lazy="<?php the_sub_field('hinh_ảnh_dự_an'); ?>" alt="<?php the_title(); ?>"></li>
                                    <?php endwhile; endif; ?>
                                </ul>
                                <ul id="thumbnail_slider01">
                                    <?php if( have_rows('slide_dự_an') ): while ( have_rows('slide_dự_an')) : the_row(); ?>
                                        <li class="thumbnail-item"><img data-lazy="<?php the_sub_field('hinh_ảnh_dự_an'); ?>" alt="<?php the_title(); ?>"></li>
                                    <?php endwhile; endif; ?>
                                </ul>
                            </div>
                            <div class="daCtInfo ow">
                                <?php the_content();  ?>
                            </div>
                        </div>
                    <!-- End content -->
                <?php endwhile; ?>
                <?php  wp_reset_query(); ?>
            <?php  endif; ?>
            
        </div>
        <div class="daCT02 ow wow fadeInUp">
            <div class="grid-1175">
                <h2><?php _e("<!--:en-->OTHER PROJECTS<!--:--><!--:vi-->DỰ ÁN KHÁC<!--:-->"); ?></h2>
                <div class="daCt02Slide">
                    <?php
                        $postType = 'du-an';
                        $taxonomyName = 'danh-muc-du-an';
                        $taxonomy = get_the_terms(get_the_ID(), $taxonomyName);
                        if ($taxonomy){
                        $category_ids = array();
                        foreach($taxonomy as $individual_category) $category_ids[] = $individual_category->term_id;
                        $args = array( 
                            'post_type' =>  $postType,
                            'post__not_in' => array(get_the_ID()),
                            'posts_per_page' => 5,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => $taxonomyName,
                                    'field'    => 'term_id',
                                    'terms'    => $category_ids,
                                ),
                            )
                        );
                        $my_query = new wp_query($args);
                        if( $my_query->have_posts() ):
                            while ($my_query->have_posts()):$my_query->the_post();?>
                                <div class="da02Item">
                                    <?php if ( has_post_thumbnail() ) { ?>
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                                    <?php } else { ?>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/da-img11.jpg" alt="<?php the_title(); ?>">
                                    <?php } ?>
                                    <div class="da02Txt">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <span class="txt">
                                            <?php echo mb_substr(get_the_excerpt(), 0, 150,'UTF-8').'...'; ?>
                                        </span>
                                    </div>
                                </div>
                            <?php endwhile;
                    endif; wp_reset_query();}?>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- End main -->
<?php get_footer(); ?>
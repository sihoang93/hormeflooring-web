<?php get_header();?>
<?php get_template_part( 'inc/header', 'hoatdong' ); ?>
<!-- Main -->
<main>
    <section id="hoatDongChiTiet">
        <div class="hdCt01 ow wow fadeInUp">
            <div class="grid-1175">
                <div class="hdCt01Rows">
                    <div class="hdCt01Left">
                        <ul class="share">
                            <li>share</li>
                            <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <?php if ( have_posts() ) :  ?>
                        <?php while ( have_posts() ) : the_post(); ?>
                        <div class="hdCt01Right">
                            <div class="hdTt ow">
                                <h1><?php the_title(); ?></h1>
                                <span class="hdDate"><?php the_time('d F, Y'); ?></span>
                            </div>
                            <div class="hdFullTxt ow">
                                <?php the_content();  ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                        <?php  wp_reset_query(); ?>
                    <?php  endif; ?>
                </div>
            </div>
        </div>
        <div class="hdCt02 ow wow fadeInUp">
            <div class="grid-1175">
                <h2><?php _e("<!--:en-->related posts<!--:--><!--:vi-->bài viết liên quan<!--:-->"); ?></h2>
                <div class="hdCtSlick">
                <?php
                    $postType = 'hoat-dong';
                    $taxonomyName = 'danh-muc-hoat-dong';
                    $taxonomy = get_the_terms(get_the_ID(), $taxonomyName);
                    if ($taxonomy){
                    $category_ids = array();
                    foreach($taxonomy as $individual_category) $category_ids[] = $individual_category->term_id;
                    $args = array( 
                        'post_type' =>  $postType,
                        'post__not_in' => array(get_the_ID()),
                        'posts_per_page' => 5,
                        'tax_query' => array(
                            array(
                                'taxonomy' => $taxonomyName,
                                'field'    => 'term_id',
                                'terms'    => $category_ids,
                            ),
                        )
                    );
                    $my_query = new wp_query($args);
                    if( $my_query->have_posts() ):
                        while ($my_query->have_posts()):$my_query->the_post();?>
                            <div class="hdCtItem">
                                <?php if ( has_post_thumbnail() ) { ?>
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                                <?php } else { ?>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hdct-img02" alt="<?php the_title(); ?>">
                                <?php } ?>
                                <div class="hdCt02Txt">
                                    <span class="hdCat"><a href="<?php bloginfo('home'); ?>/danh-muc-hoat-dong/<?php echo $individual_category->slug; ?>"><?php echo $individual_category->name; ?></a></span>
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <span class="txt">
                                        <?php echo mb_substr(get_the_excerpt(), 0, 80,'UTF-8').'...'; ?>
                                    </span>
                                    <span class="hdDate"><?php the_time('d F, Y'); ?></span>
                                </div>
                            </div>
                        <?php endwhile;
                endif; wp_reset_query();}?>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- End main -->
<?php get_footer(); ?>
<?php get_header();?>
<div class="breaKum nwDD">
    <div class="grid-1665">
        <a href="<?php bloginfo('home'); ?>">Trang chủ /</a> <a href="<?php bloginfo('home'); ?>/tin-tuc/">Tin tức /</a>
        <?php the_title(); ?>
    </div>
</div>

<div id="bannerPage" style="background-image: url(./images/n-img02.jpg);" class="ttDetai00">
    <div class="grid-1665">
        <div class="bannerInfo">
        </div>
    </div>
    <span class="imgBannerSp"><img src="./images/psp-img02.jpg" alt="Phú Cường Auto xin kính chào quý khách"></span>
</div>
<!--end /#bannerPage-->

<!-- Main -->
<main>
    <section id="tintucChiTiet">
        <div class="grid-1665">
            <?php if ( have_posts() ) :  ?>
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="ttChiTietRows01 wow fadeInUp">
                <div class="ttCR01">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <?php } else { ?>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/n-img02.jpg"
                            alt="<?php the_title(); ?>">
                    <?php } ?>
                </div>
                <div class="ttCR02">
                    <h2><?php the_title(); ?></h2>
                    <div class="ttCR02FullTxt">
                        <?php the_field('mo_tả_ngắn'); ?>
                    </div>
                </div>
            </div>
            <!--end /.ttChiTietRows01-->
            <div class="ttChiTietRows02 wow fadeInUp">
                <?php the_content(); ?>
            </div>
            <?php endwhile; ?>
            <?php  wp_reset_query(); ?>
            <?php  endif; ?>
            <!--end /.ttChiTietRows02-->
            <div class="ttChiTietRows03 wow fadeInUp">
                <h2>Tin liên quan</h2>
                <div class="tt03Rows">
                <?php
                $categories = get_the_category(get_the_ID());
                if ($categories){
                $category_ids = array();
                foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
                $args=array(
                'category__in' => $category_ids,
                'post__not_in' => array(get_the_ID()),
                'posts_per_page' => 4, // So bai viet dc hien thi
                );
                $my_query = new wp_query($args);
                if( $my_query->have_posts() ): while ($my_query->have_posts()):$my_query->the_post();
                ?>
                    <div class="tt03Item">
                    <a href="<?php the_permalink() ?>"></a>
                    <span class="tt03IImg">
                        <?php if ( has_post_thumbnail() ) { ?>
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                            <?php } else { ?>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/n-img03.jpg"
                                alt="<?php the_title(); ?>">
                        <?php } ?>
                    </span>
                    <h3><?php the_title(); ?></h3>
                    </div>
                <?php
                endwhile;
                endif; wp_reset_query();
                }
                ?>
                </div>
            </div>
            <!--end /.ttChiTietRows03-->
        </div>
    </section>
    <!--end /#tintucChiTiet-->
    <?php get_footer(); ?>